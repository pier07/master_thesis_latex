#Sensor Fusion of Raw GPS Measurements for Autonomous Vehicle Localization
---
##Motivations (& Objectives)
* importance of navigation
* motivation: cargo ants
* "The work of this Master's Thesis is in the context of outdoor localization of autonomous vehicles. The aim of this project is to use raw GPS pseudorange data in the localization problem by creating the corresponding geometric constraint for each measurement."

---
###struttura della presentazione
in cui spiego come si divide il lavoro/presentazione.

* intro a SLAM
* gps
* impl 1: gps data processing
* sensor fusion implementation
* conclusion

---
##Cargo-ANTs Project
* sigla
* problem
* partners
> "cargo ants represents a use case istance of the work developed through this master's thesis"

---
##SLAM
* definizione
* applicazioni
* definizione matematica

---
##Sensor fusion
* pro

---
##Wolf
* sigla
* etc
* tree

---
## GPS

#### fundamentals

#### trilateration

#### signals

---
##IMPL1: GPS data processing

####overview della soluzione implementata
* intro a ros
* schemino

####trilaterazione con ceres
* intro a ceres

####satellite position from ephemeris
* gpstk
* rinex 

####septentrio asterx1




