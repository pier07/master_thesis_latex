\chapter{Software Development I. GPS Data Processing}
\label{ch:impl_gps}
\thispagestyle{chiara}

In this chapter we will present the implementation of the satellites' orbit estimation and the trilateration of the user position.
The programming language chosen for this project is C++, for performance reason and because of compatibility with other tools that we will use. One of these tools is ROS, and now we are going to introduce it. This will help the reader to understand the architecture of this project solution.

\bigskip

%%% INTRO A ROS
\textbf{ROS} (Robot Operating System)~\cite{ros-site} is a framework for robot software development. It is a collection of tools, libraries, and conventions that aim to simplify the task of creating complex and robust robot behavior across a wide variety of robotic platforms. Its libraries are in C++ and Python, but also other languages are supported. ROS is an open source software released under the terms of the BSD license. It was originally developed in 2007 by the Stanford Artificial Intelligence Laboratory, and now is maintained by the Open Source Robotics Foundation.

A key concept in ROS are the nodes. A node is a process that performs some computation. A node can publish or subscribe to data streams called topics, which allow communication between them.
A robot application usually is composed by many nodes, each one responsible of a different aspect.
The use of nodes in ROS provides several benefits to the overall system, like improving fault tolerance and diminishing the code complexity.

One relevant node provided by ROS is RVIZ. This node is specialized in visualization. It can visualize various data format that are published from other nodes through topics, such as coordinates frames and visual markers to visualize the robot and the surrounding environment.

\bigskip

In order to have a general overview of the contents that are presented later on in this chapter, in \figurename~\ref{fig:schema_nodi_tutto} we illustrate the final architecture, contained in the package \verb+raw_gps_ros+, and in the following we will briefly describe the goal of each node.

% https://www.draw.io/?state=%7B%22ids%22:%5B%220B-ATQgwEFmKgVjQ3alhPUlRTb00%22%5D,%22action%22:%22open%22,%22userId%22:%22103950164066593583565%22%7D#G0B-ATQgwEFmKgVjQ3alhPUlRTb00
\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=\textwidth]{images/implementazione/schema_nodi.pdf}
        \cprotect\caption{Architecture of the \verb+raw_gps_ros+ package. The arrows without names represent sets of relations, that are detailed in Figures~\ref{fig:schema_nodo_orbit} and~\ref{fig:schema_driver_node}. }
        \label{fig:schema_nodi_tutto}
    \end{center}
\end{figure}

\verb+raw_gps_ros+ can use alternatively two kinds of inputs. 
The first option is to read the raw GPS data stored in specific datasets, called RINEX files (see section~\ref{sec:orbit}).
The second option is to capture it directly through a real GPS receiver, that is controlled from \verb+driver_node+ (see section~\ref{sec:driver}).

The raw GPS data is processed respectively from \verb+rinex_processor+ and \verb+raw_gps_processor+.
These nodes publish in the \verb+/sat_pseudoranges+ topic a vector of satellite positions and their corresponding pseudoranges for every epoch.

On the top right we can see the \verb+trilaterator+ node. Its task is to listen to the \verb+/sat_pseudoranges+ topic and, every time some pseudoranges are published, starting to trilaterate the user position. As soon as it computes a position and a clock bias, it publishes the result in the \verb+/estimated_position+ topic.

Another node, called \verb+viz_manager+, listens to all the topics and produces the visualization objects, that will be visualized through the \verb+rviz+ node.

%TODO mostra struttura con rqt\_graph?

%c'è anche un receiver-sim-node che simula una spirale e mostra la differenza tra trilateratione e posizione reale.. questa mi sembra desse buoni risultati!!
%(in input vuole la posizione dei satelliti, lui simula un receiver che parte da 0 e fa una spirale)



\section{Trilateration with Ceres-Solver}
\label{sec:ceres}
%parla della trilateratione semplice
To trilaterate the user position as described in section \ref{ssec:math} we have to optimize the non-linear least squares problem described in equation \ref{eq:trilat}.
To solve this problem efficiently we use a specific solver called Ceres-Solver.

\bigskip

\textbf{Ceres-Solver} is a library for modeling and solving large, complicated optimization problems~\cite{ceres-solver}.
It is an open source C++ library created by Google in 2010 in order to estimate the pose of Street View cars and aircrafts. Ceres is still used by Google and other important companies and during its life span it has been extensively tested and optimized. The code is portable and can run on different operating systems, like Linux, Windows, Mac OS X, Android and iOS.

%TODO altro da http://ceres-solver.org/features.html ?
%tipo potrebbe essere interessante ``Derivatives''(e nomina i template, per avere l'auto differenziazione), ``Local Parameterization'', ``Solver Choice'', ``Covariance estimation''


%sorgente http://ceres-solver.org/nnls_tutorial.html
Ceres accepts non-linear least squares problems with bounds constraints, structured like
\begin{equation}
 \operatorname*{min}_{\mathbf{x}} \frac{1}{2} \sum_i \xi_i \left( \parallel f_i \left( x_{i_1} , ... , x_{i_k} \right) \parallel^2  \right)     \qquad \qquad     l_j \leq x_j \leq u_j
\end{equation}

where $\xi_i \left( \parallel f_i \left( x_{i_1} , ... , x_{i_k} \right) \parallel^2  \right)$ is called \verb+ResidualBlock+ and is composed with the \verb+CostFunction+ $f_i \left( \cdot \right)$ and the \verb+LossFunction+ $\xi_i$.
The \verb+LossFunction+ is used to reduce the influence of the outliers on the solution of non-linear least squares problems.

As we saw in section \ref{ssec:math}, the positioning problem is exactly in the same form of ours (see equation \ref{eq:trilat}), with:

\begin{equation}
 \verb+LossFunction  + \xi_i \equiv 2
\end{equation}
\begin{equation}
 \verb+CostFunction + f_i \left( \cdot \right) =   \sqrt{ (x_j - x)^2 + (y_j - y)^2 + (z_j - z)^2 } + c \cdot t - \rho_j
\end{equation}

\bigskip

The first step to find an optimized solution through Ceres is to write a cost functor class, that will be used by the solver to evaluate our formula.

The core of this class is the \verb+operator()+ method, in which the error is estimated subtracting the real measurement from the expected measurement, and saved in \verb+residual[0]+.
It is important to notice that \verb+operator()+ is a templated method, and \verb+T+ can be a double or a special type called \verb+Jet+ that let us use the automatic differentiation offered by Ceres, without having to write manually the Jacobian computation code.

\vspace{2cm}

\begin{lstlisting}[linewidth=0.98\textwidth, breaklines, language=c++]
class CostFunctor
{
public:
 CostFunctor(const SatelliteMeasurement sm_, const double speed_) : sm(sm_), speed(speed_) 
 {}

 template <typename T>
 bool operator()(const T* const pos, const T* const bias, T* residual) const
 {
   T square_sum = T(0);
   
   for (int i = 0; i < sm.pos.coords.size(); ++i)
     square_sum += pow(pos[i] - T(sm.pos.coords[i]) , 2);
   
   T distance = (square_sum != T(0)) ? sqrt(square_sum) : T(0);
   residual[0] = (distance + bias[0]*speed) - sm.pseudorange;
   return true;
 }

private:
 SatelliteMeasurement sm;
 const double speed;
};
\end{lstlisting}
\bigskip


When we want to trilaterate a position we create a \verb+ResidualBlock+ based on this \verb+CostFunction+ class for each satellite visible in that moment, and then we ask the optimizer to try to find the solution that minimizes the sum of errors of all pseudoranges currently measured.

To test the trilateration through Ceres, we have created a library called \emph{Trilateration}.
To trilaterate a position are needed at least four satellites and the pseudoranges between the receiver and them. To specify that data it is possible to directly insert  in the command line each satellite's position $\bm{s}$ and pseudorange $\rho$. Instead of specifying the pseudoranges, it is possible to insert the real receiver position $\bm{u}$, the clock bias $t_b$ and a value for the standard deviation $\sigma$ of the noise.
If the second input type is chosen, the pseudoranges $\rho_j$ will be simulated considering the true range $ r_j = \|\bm{r}_j\|$ between the \emph{j}-th satellite and the real receiver, the clock bias $t_b$ and a Gaussian white noise $\mathcal{N}(0, \sigma^2)$ will be applied.
\begin{equation}
 \rho_j = r_j + c \cdot (t_b + t_n) \ ; \quad \quad t_n \sim \mathcal{N}(0, \sigma^2)
\end{equation}

After this step the pseudoranges will be available, and it is possible to start to build the problem and then optimize. A \verb+ResidualBlock+ is added to the problem for each satellite.
%TODO parla delle opzioni  di ceres
The minimization behaviour chosen to be used from the solver is the \emph{Trust Region}.
This approach approximates the objective function using a quadratic model function over a subset of the search space, called trust region. If the model function applied lower the cost of the objective function the trust region expanded, otherwise is contracted. %more details at http://ceres-solver.org/nnls_solving.html#Solver::Options::linear_solver_type__LinearSolverType

To help the convergence of the solution it is possible to provide an initial guess of the solution, and this leads to more fast convergence and accurate results.

%\hl{I'll do  a simulation + analysis back in the lab, because i can try with better values. everything depends also on the std dev of the noise.}

In \figurename~\ref{fig:trilat_lib_result} we can see the output screen of an execution of the trilateration. At the beginning it simulates some pseudorange measurements, knowing that the real receiver's position is $\bm{u} = (200, 200, 20)$ and clock bias $t_b = 2.5 ms$, and applying a white Gaussian noise $\mathcal{N}(0, \sigma^2)$ with standard deviation $\sigma = 5 \cdot 10^{-9}$ to the clock bias.
After that Ceres begins the optimization, starting from an initial guess of $\bm{u} = (0, 0, 0)$ for the receiver's position and $1 \mu s$ of clock bias.
After twelve iterations the result converges, leading to a position that is $0.57 m$ far from the real position. The estimated clock bias is almost perfect, given that the original one is $2.5 ms$ and the estimated one is only $1.07 ns$ more.

\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=\textwidth]{images/implementazione/trilat_5-9.png}
        \caption{Output screen of a test of the trilateration library.}
        \label{fig:trilat_lib_result}
    \end{center}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Compute Satellite Position from Ephemeris}
%\hl{old title: Satellite positioning with GPSTk and satellite nav message}
\label{sec:orbit}
%TODO mostra struttura con rqt\_graph?

%parla di come stimi l'orbita dei satelliti
A fundamental step in the user positioning process is to calculate the position of the satellites in a certain moment, usually when a GPS observation has been sent. As described in subsection~\ref{ssec:nav_msg}, the information needed to compute this data is sent in the GPS navigation messages.
To calculate the orbit of a single satellite we use a C++ library called GPSTk.

\bigskip

%%% INTRO A GPSTk
\textbf{GPSTk} (GPS Toolkit)~\cite{gpstk_paper} is an open source C++ library that provides a wide range of functions that solve processing problems associated with GNSS, such as manage and convert time representations, ephemeris calculations, compute a position solution and processing standard formats like RINEX.
The GPSTk is developed by Space and Geophysics Laboratory, within the Applied Research Laboratories at the University of Texas at Austin.

%%% COME STIMO L'ORBITA?
With GPSTk we can compute the satellites' position in a certain moment of the day, given that we know the corresponding ephemerides.
In this library ephemerides are stored in objects of the  \verb+GPSEphemeris+ class. Each object contains all the parameters needed to compute the evolution of a single satellite in its orbit.

In the following of this section we will discuss about how to fill these objects, because it can be done in different ways and needs a longer discussion.

From each ephemeris it is possible to compute the corresponding satellite position at the given time through the method \verb+GPSEphemeris::svXvt(time)+. This method implements the equations of orbital motion as defined in IS-GPS-200~\cite{is-gps}. 
It returns an \verb+Xvt+ object that contains the satellite's Earth-Centered, Earth-Fixed Cartesian position, velocity, clock bias and drift. From now on we will refer to this Position, Velocity and Time  object as PVT.

%% intro a RINEX
Before explaining how to fill the ephemerides, it is necessary to introduce the RINEX file format.

\bigskip

\textbf{RINEX} (Receiver Independent Exchange Format)~\cite{rinex} is a data interchange format for raw satellite navigation system data. RINEX can store data from different GNSS, like GPS (including GPS modernization signals e.g. L5 and L2C), GLONASS, Galileo, Beidou, etc. The RINEX standard allows to store data in a format independent from the receiver used. This allows the user to easily exchange the receiver device and also to post-process it with other data unknown to the original receiver, like using an accurate atmospheric model.

Three different RINEX file types exist: Observation data file, Navigation message file and Meteorological data file.
Each file type consists of a header section and a data section. The format has been optimized for minimum space requirements and there is no maximum record length limitation for the observation records.

Observation data file contains a list of observation messages as described in section~\ref{ssec:obs_data}.

Navigation message file contains a list of all the epochs in which observations are made and the corresponding ephemerides. In order to keep the file small, ephemerides are saved only when a new satellite is seen or when they are no more valid, and not every thirty seconds as they are sent by satellites.

Meteorological data file contains information about the weather. They can not be produced by the receiver itself, but they must be provided by an external source. The use of this data is out of the scope of this project.

\bigskip

%%% COME RIEMPIRE GLI OGGETTI EPHEMERIS?
We can fill an ephemeris reading from a RINEX navigation file or receiving data directly from a GPS sensor.
%% tramite rinex nav file
With GPSTk it is straightforward to read ephemerides contained in a RINEX nav file, by means of using the class \verb+Rinex3NavStream+ and extract all the ephemerides through the overloaded \verb+operator<<()+.
%% tramite real gps sensor
Instead, if we want to use a real device, we need a GPS sensor capable to provide the raw data received from the satellites, and not only the GPS fix. In this work we used a Septentrio AsteRx1 GPS/Galileo Single-frequency receiver.
In section~\ref{sec:driver} we will discuss about the custom driver needed to publish the raw data and fill a \verb+GPSEphemeris+ object.

% https://www.draw.io/?state=%7B%22ids%22:%5B%220B-ATQgwEFmKgb0JIUDdrZ3pCbU0%22%5D,%22action%22:%22open%22,%22userId%22:%22103950164066593583565%22%7D#G0B-ATQgwEFmKgb0JIUDdrZ3pCbU0
\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=0.8\textwidth]{images/implementazione/schema_nodo_orbit.pdf}
        \cprotect\caption{Architecture of \verb+orbit_estimation+ node that reads data from RINEX files.}
        \label{fig:schema_nodo_orbit}
    \end{center}
\end{figure}

In our representation through the ROS node RVIZ there is a frame called world, that represent the center of the Earth in the ECEF coordinates. Around this point we can see a green sphere that represents the Earth, to better visualize the satellites' evolution. In \figurename~\ref{fig:orbit_general} we can see eight satellites, each of them in its own frame, moving around the Earth. The light blue arrows represent the velocity of each space vehicle.
The red line we can see behind one of the satellites is its orbit. It doesn't appear circular only because the ECEF coordinate system is not inertial, since it moves with the Earth rotation.

\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=1\textwidth]{images/implementazione/orbit_general.png}
        \caption{Representation on RVIZ of the satellite positioning around the Earth.}
        \label{fig:orbit_general}
    \end{center}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Custom Driver for Septentrio AsteRx1}
\label{sec:driver}
%TODO for more info, see this source \url{http://wiki.iri.upc.edu/index.php/AstRX1_GPS_driver}

The device we used to get the real GPS data is a Septentrio AsteRx1 GPS/Galileo single-frequency receiver. 
It is composed by a compact OEM board with low power consumption integrated in the receiver platform and an active high-gain antenna. These components can be seen in \figurename~\ref{fig:asterx}, the receiver platform on the left and the external antenna on the right, respectively.
The antenna is external so it can be placed in the best position to guarantee visibility of satellites.
The platform provides an USB interface that allows to connect AsteRx1 to a computer. 
Once connected, the receiver can be configured through a command line interface~\cite{asterx-command}, and it is compatible both with Linux and Windows operating systems.

\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=0.9\textwidth]{images/implementazione/asterx1_crop.jpg}
        \caption{Septentrio AsteRx1 GPS/Galileo single-frequency receiver. On the left there is the receiver device and on the right the external antenna.}
        \label{fig:asterx}
    \end{center}
\end{figure}

The binary output format of Septentrio receivers is SBF, that straightforwardly comes from Septentrio Binary Format~\cite{asterx-sbf}. In this format, data is arranged in binary blocks. Copious different SBF blocks exist, but we are interested only in few of them.

This receiver contains 24 hardware channels for simultaneous tracking all the visible GPS and Galileo satellites. Each visible satellite in a certain moment is automatically allocated in a channel.

For each measurement epoch, AsteRx1 creates an output SBF block called \verb+MeasEpoch+ that contains a measurement for each tracked satellite. All the measurement set of a single \verb+MeasEpoch+ block refers to the same time.

The reference time in AsteRx1 is kept as close as possible to the currently used GNSS time, in our case GPS time. Anyway, as explained in subsection \ref{ssec:math}, the receiver's clock irremediably contains a time bias with respect to the GPS time.
AsteRx1's time is stored in the SBF block \verb+TimeStamp+  and it is composed by two numbers: the time-of-week \verb+TOW+ and the week number counter \verb+WNc+.
\verb+TOW+ is expressed in milliseconds and refers to the milliseconds elapsed from the beginning of the current week, and the \verb+WNc+ refers to the number of complete weeks elapsed since January 6, 1980.

\bigskip

The GPS fix produced by AsteRx1 is contained in \verb+PVTCartesian+ and \verb+PVTGeodetic+ blocks, that respectively refer to ECEF coordinate system and latitude, longitude and altitude in the Geodetic coordinate system.

Anyway, for the scope of our project, we want to focus on the raw data sent by GPS satellites and not on the fix produced by the receiver.

In order to get the raw data from AsteRx1, a custom driver was needed.
This driver uses the command line interface provided by Septentrio in order to retrieve the desired data, and offers an API to manage the receiver from outside~\footnote{The code of this low-level driver can be found at  \url{https://devel.iri.upc.edu/labrobotica/drivers/asterx1_gps}}. 
We will refer to this part of the driver as the \emph{low-level driver} for the GPS receiver.

To use the GPS receiver in our architecture, a ROS node has been created. This node wraps the low-level driver and, using the provided API, receives data from AsteRx1 and publishes the information we need in different topics. In \figurename~\ref{fig:schema_driver_node} we can see the driver node, called \verb+driver_node+, and its most significant topics.
The \verb+/gps+ and \verb+/gps_ecef+ topics contain a ROS message with the GPS fix computed by AsteRx1, respectively in Geodetic coordinates and ECEF.
\verb+/gps_meas+ contains the observation data described in subsection~\ref{ssec:obs_data}.


%https://www.draw.io/?state=%7B%22ids%22:%5B%220B-ATQgwEFmKgU2lMZHVpY3Rjalk%22%5D,%22action%22:%22open%22,%22userId%22:%22103950164066593583565%22%7D#G0B-ATQgwEFmKgU2lMZHVpY3Rjalk
\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=0.88\textwidth]{images/implementazione/schema_driver_node.pdf}
        \caption{Architecture of the driver node for the GPS receiver AsteRx1.}
        \label{fig:schema_driver_node}
    \end{center}
\end{figure}

To compute the satellite position in a certain moment, the corresponding ephemeris must be known. AsteRx1 can publish a GPS decoded message with clock and ephemeris data through the SBF block \verb+GPSNav+.
This block contains more than forty parameters needed to reconstruct the ephemeris.
The ROS node \verb+driver_node+ publishes this set of parameters in the \verb+/gps_nav+ topic. 
As we wrote in the previous section, another node will listen to this topic in order to build the GPSTk's \verb+GPSEphemeris+ object.
To construct it, all the parameters contained in the \verb+GPSNav+ block must be assigned to the \verb+GPSEphemeris+ object.

After some weeks of attempts without good result, we found out that GPSTk permits to construct another ephemeris object, \verb+EngEphemeris+, using the first three sub-frames of the navigation data (see \ref{ssec:nav_msg}).
For this reason we changed the low-level driver in order to publish the raw navigation data in the \verb+/gps_raw+ topic.

Each sub-frame is composed by ten words of 30 bits each. To construct the \verb+EngEphemeris+ we need to instantiate an empty object and call the method \verb+addSubframe(..)+ for three times to add the sub-frames. After that it is possible to convert the \verb+EngEphemeris+ into a \verb+GPSEphemeris+ and use it in the same way as one read from a RINEX navigation file.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Computing GPS Fix from Pseudoranges}
Now that we know how to trilaterate with Ceres and how to estimate the satellite's position in a certain moment, we can compute a GPS fix from the data received from satellites. In order to do it, firstly it is necessary to compute the satellite positions from ephemerides, and then to apply a correction to the received pseudoranges.
Now, if we have four or more satellite we can prepare the Ceres-problem as described in the section~\ref{sec:ceres} and then find an optimized solution.
and then it is possible to trilaterate. In the following of the section we will explore these steps.

 \subsection{Correct Raw Pseudorange Measurements}
 \label{ssec:pseudorange_correction}
 Every time we receive a GPS observation, that means a vector of satellites and their respective pseudoranges, we have to check if we have already received the corresponding ephemeris in order to calculate the satellite's position. If the correct ephemeris is unknown we cannot use that satellite, and we simply discard it.

 Anyway, this process is not as straightforward as it seems, because we need the satellite position at the transmit time $t_{Tx}$, and with the GPS observation received we only have the receiving time $t_{Rx}$.
 To calculate it precisely, we have a two-step process: we firstly calculate a raw estimation of the transmit time $t_{Tx}^1$:
 %
 \begin{equation}
  t_{Tx}^1 = t_{Rx} - \frac{\rho_{raw}}{c}
 \end{equation}
 %
 Now we need to get the PVT of the satellite in that moment:
 %
 \begin{equation}
  PVT^1 = svXvt(t_{Tx}^1)
 \end{equation}
 %
 From this first guess of $PVT$, $PVT^1$, we can obtain the satellite's clock bias and calculate the relativistic clock correction.
 The rate of advance of two identical clocks, placed respectively one  on the terrestrial surface and the other in the satellite, will differ due to the difference of the gravitational potential (general relativity) and to the relative speed between them (special relativity)~\cite[p. 306]{understanding_gps}.
 %http://www.navipedia.net/index.php/Relativistic_Clock_Correction

 The relativistic clock correction $t_{\gamma}^{PVT^1}$ can be calculated as
 %
 \begin{equation}
  t_{\gamma}^{PVT^1} = -2 \cdot \frac{\textbf{s}^{PVT^1} \cdot \textbf{v}^{PVT^1}}{c^2}
 \end{equation}
 %
 where $\textbf{s}^{PVT^1} = PVT^1.pos$ and $\textbf{v}^{PVT^1} = PVT^1.vel$ refer respectively to the satellite position $(m)$ and velocity $(m/s)$ vectors.

 Using the satellite's clock bias $t_b^{PVT^1} = PVT^1.clockBias$ we can calculate the true transmit time $t_{Tx}$:
 %
 \begin{equation}
  t_{Tx} = t_{Tx}^1 - t_b^{PVT^1} - t_{\gamma}^{PVT^1}
 \end{equation}
 %
 and finally the satellite PVT at that time:
 %
 \begin{equation}
  PVT = svXvt(t_{Tx})
 \end{equation}
 %se alla fine ho un satId negativo vuol dire che non ha trovato in questo punto
 From the $PVT$ we can correct the raw pseudorange received considering the satellite's clock bias and the relativistic clock correction.
 %
 \begin{equation}
  \rho_{corrected} = \rho_{raw} + c \cdot (t_b^{PVT} + t_{\gamma}^{PVT})
 \end{equation}
 %

 \subsection{Receiver Autonomous Integrity Monitoring}
 \label{ssec:raim}
 
 Now that we have calculated the corrected pseudorange we could directly publish the satellite positions and pseudoranges and let the trilateration node work.
 Anyway, if we have at least five satellites in line of sight, we can apply a Receiver Autonomous Integrity Monitoring (RAIM) algorithm.

 \bigskip
 
 \textbf{RAIM} is a technique that uses an overdetermined solution to perform a consistency check on the satellite measurements.
 %http://www.navipedia.net/index.php/RAIM
 The RAIM algorithm compares the smoothed pseudorange measurements among themselves to ensure that they are all consistent.
 A pseudorange that differs significantly from the expected value may indicate a fault of the associated satellite or another signal integrity problem usually caused by ionospheric dispersion~\cite[p. 347]{understanding_gps}.

 If an outlier is found, it can be detected and excluded. GPSTk offers an implementation of this algorithm in \verb+PRsolution2+.
 %TODO scrivere se sto algoritmo effettivamente migliora le cose o no!

 RAIM algorithm is used by both our GPS data processors nodes and the GPS receiver AsteRx1~\cite{asterx-firmware}.
 
 
 \subsection{Trilaterate from Real Data}
 %%%ROS RECEIVER NODES
 Pseudoranges correction and RAIM technique are implemented in the ROS nodes \verb+rinex_processor+ and \verb+raw_gps_processor+. 
 They act as receivers, with the only difference that one reads the raw data from RINEX files and the other one receives it from the driver node as described in section~\ref{sec:driver}.
 When they receive a GPS measurement they calculate the satellite position in ECEF coordinates, correct the pseudoranges and eventually apply RAIM algorithm. In the end they publish a vector of satellite measurements in a topic called \verb+/sat_pseudoranges+ and they keep listening for new observations (see \figurename~\ref{fig:schema_nodi_tutto}).

 %%% TRILATERATION NODE
 One of the nodes that subscribes the \verb+/sat_pseudoranges+ topic is the node \verb+trilaterator+. 
 It uses the trilateration library described in section \ref{sec:ceres} to estimate the receiver's position and bias and publish this results to \verb+viz_manager+ node, that will visualize all the data it received through RVIZ.


%\clearpage

\section{Trilateration Results}
\label{sec:results_gps}


In this section we analyze the estimation of the user position done by the \verb+trilaterator+ node, using the raw pseudorange measurements and ephemerides processed by the processor nodes previously described.
\figurename~\ref{fig:gps_map_est} shows the results obtained, with a blue dot for every GPS fix computed~\footnote{Figures~\ref{fig:gps_map_est} and~\ref{fig:gps_map_real} are created using an on-line utility called GPS Visualizer \url{http://www.gpsvisualizer.com/}}.
\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=\textwidth]{images/implementazione/sciantamejo/est_scianta++.png}
        \cprotect\caption{GPS fixes produced from raw GPS measurements by \verb+raw_gps_ros+'s nodes.}
        \label{fig:gps_map_est}
    \end{center}
\end{figure}

To compare the results obtained, in \figurename~\ref{fig:gps_map_real} are shown the GPS fixes produced directly by the receiver, doing the same path.
\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=\textwidth]{images/implementazione/sciantamejo/real_scianta++.png}
        \caption{GPS fixes produced directly by AsteRx1 receiver.}
        \label{fig:gps_map_real}
    \end{center}
\end{figure}
As we can see, the red path is much more neat compared to the blue one, but the shape of the entire path is correct.

To produce such a smooth path AsteRx1 receiver uses a series of techniques to filter results~\cite{asterx-firmware}. 
One of these is the dynamic of the receiver, that is set to pedestrian. This means that the receiver for every new estimation will check the distance traveled, using the position of the last known fix as a reference.
If the distance between these two spots is not compatible with the distance a pedestrian can cover in the same time, the estimation will be considered faulty.

Furthermore, AsteRx1 takes advantages of GNSS augmentation such as Satellite-Based Augmentation System (SBAS)~\cite{understanding_gps, sbas} to increase accuracy of the satellite positioning.

The only outlier rejection technique we are applying is the RAIM algorithm described in section~\ref{ssec:raim}.
Analyzing the behaviour of the obtained results we noticed that our solution, but also the GPS fix produced by the receiver itself, are very sensible to multipath interference, and this is the main cause of outliers.

We created a three dimensional representation of the two path overlapping, using the \verb+viz_manager+ node and \verb+RVIZ+. In \figurename~\ref{fig:gps_2d_alto} we can see the two paths overlapped, to better understand the differences.
\begin{figure}[!tb]
    \begin{center}
        \includegraphics[width=\textwidth]{images/implementazione/sciantamejo/2d_alto_bianco.png}
        \caption{Three-dimensional representation of the two paths. In blue it is represented the path estimated by our implementation, in red the path estimated by the receiver. The size of the image is proportional to the grid, whose cells are squares with sides of 10 meters.}
        \label{fig:gps_2d_alto}
    \end{center}
\end{figure}
On two dimensions the estimated positions are close to the GPS fix, with some estimate that distance itself a pair of meters.

The least accurate estimation regards the altitude, as we can see from \figurename~\ref{fig:gps_altitude}. Altitude is always the flaw of the GPS positioning because of the geometrical structure of the problem. 
To overcome to this weak point, the software of commercial GPS receivers reference altitude measurements to the altitude of the ground in the geodetic model of the Earth and on the last known position~\cite{gps_altitude}.

\begin{figure}[!tb]
    \begin{center}
        \includegraphics[width=\textwidth]{images/implementazione/sciantamejo/altitude_grid.png}
        \caption{Lateral view of the estimated paths, to show the altitude flaw. The size of the image is proportional to the grid, which lies on the $XZ$ plane, and whose cells are squares with sides of 5 meters.}
        \label{fig:gps_altitude}
    \end{center}
\end{figure}

