\chapter[Software Development II. Wolf Specialization]{Software Development II. Wolf\\ Specialization}
\label{ch:impl_wolf}

\thispagestyle{chiara}

\section{Wolf Integration}
\label{sec:wolf_integration}
The second main step of the work for this thesis has been to integrate the GPS functionalities in Wolf (see section \ref{sec:wolf_teoria}). To do that, it has been necessary to specialize most of the base classes present in the Wolf tree.

A \verb+SensorGPS+ class has been created to represent the GPS sensor, from the point of view of the SLAM problem. In this class there are all the parameters needed to describe a GPS sensor. Among them, there is the sensor position, that represents the mounting point of the GPS antenna with respect to the robot.
The only intrinsic parameter of a GPS receiver is its clock bias with respect to the GPS time.
Two other values saved in this class are the position and orientation of the \emph{Map} frame. The concept of frame is very important, and in the next section will be exhaustively explained.

With the GPS sensor, it is associated a signal processor, implemented in the \verb+ProcessorGPS+ class. This class is mainly composed by a method to process a GPS capture, extract the features from there and create from each feature a constraint for the optimization problem.
In the GPS case, a \verb+CaptureGPS+ corresponds to a GPS observation with a vector of pseudoranges and satellite positions, as published from the \verb+raw_gps_processor+ or the \verb+rinex_processor+ nodes. That means that every satellite position and pseudorange received are already corrected, as explained in subsection~\ref{ssec:pseudorange_correction}.

In the GPS case, we chose to create a different feature for every received pseudorange, so the class \verb+FeatureGPSPseudorange+ contains only a three-dimensional vector for the satellite position and a real number for the pseudorange.
To each feature it is associated a single constraint. 
The class \verb+ConstraintGPSPseudorange2D+ is the most complex class of the specialized one for the GPS, as it is the templated class that have to work with Ceres-Solver. Basically, the operations that must be done to compute the residual are the same implemented in the \verb+trilateration+ library and described in section~\ref{sec:ceres}.
The small but substantial difference is that we don't know and don't want to estimate the sensor position in the ECEF coordinates, but we want to localize the robot with respect to the Map.

In the next section we will introduce the concept of reference frame and how a set of these frames is structured in our work. After that we will show the important difference between the simple \verb+operator()+ of the implementation in the node described in the chapter~\ref{ch:impl_gps} and the \verb+operator()+ in Wolf.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Reference Frames Structure}
\label{sec:frames}

%% GENERIC
An essential concept in mobile robotics is the notion of reference frame. A reference frame consists of an abstract coordinate system and it is uniquely fixed in the space by the position of its origin of coordinates and its orientation with respect to its \emph{parent frame}. 
The parent of all the frames is called \emph{global frame} and it is considered static and fixed somewhere in the space.

The position $\bm{p}_B^F$ and orientation $\bm{\Phi}_B^F$ of a rigid body $B$ relatively to the frame $F$ together are called \emph{pose} of $B$ in $F$, which is noted as $B^F = (\bm{p}_B^F, \bm{\Phi}_B^F)$.

In the three-dimensional space, a position $\bm{p}$ is a 3D point and an orientation $\bm{\Phi}$ can be represented with a quaternion or with a 3x3 rotation matrix.

%% OUR PROBLEM
In our problem, we can identify four different frames. The global frame is called \emph{ECEF} and corresponds to the ECEF coordinate system.
A robot moves into a limited area of the world. This area is represented through a frame named \emph{Map}, and it is a child of the \emph{ECEF} frame, as we can see in \figurename~\ref{fig:frame_ecef}.

\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=0.7\textwidth]{images/impl_wolf/frame_world.pdf}
        \caption{\emph{Map} and \emph{ECEF} frames. $T^E_M$ transforms the relative positioning of the robot with respect to the Map to an absolute positioning on Earth, in ECEF coordinates.}
        \label{fig:frame_ecef}
    \end{center}
\end{figure}

This transforms the relative positioning of the robot with respect to the Map to an absolute positioning on Earth, in ECEF coordinates. 

Inside this Map we can see the robot's frame, called \emph{Base} frame, that represents the robot position and orientation with respect to the Map. In turn, \emph{Base} frame is the reference frame of all the sensor mounted in the robot, and in our case we have another frame called \emph{GPS}. In the GPS case particularly, the orientation of this frame is not important, because a GPS antenna is omnidirectional.

If \figurename~\ref{fig:frame_map} we can see in detail the Map and the trajectory the robot had respect to \emph{Map} frame.

\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=\textwidth]{images/impl_wolf/frame_map.pdf}
        \caption{Trajectory of the robot inside the Map.}
        \label{fig:frame_map}
    \end{center}
\end{figure}



Now we can see the substantial difference between this situation and the simpler problem of localizing the GPS antenna directly in the ECEF coordinates.
In this latter case we know the sensor position with respect to the \emph{Base} frame, and we have to express it in the \emph{ECEF} frame.

Formalizing mathematically this problem, we can call the GPS sensor position with respect to the \emph{Base} frame $\bm{S}^B$, and we want to calculate it with respect to the \emph{ECEF} frame, $\bm{S}^E$.

This frame transformation can be done using quaternions or homogeneous transformation matrices, and we chose the latter option.
Such homogeneous transformation matrix for 3D points is a 4x4 matrix composed like this:
$$
\bm{T}=
  \begin{bmatrix}
    \bm{R}  &    \bm{t} \\
    \bm{0}  &    1 
  \end{bmatrix}
$$
where $\bm{R}$ is the rotation matrix and $\bm{t}$ the translation of the origin.
Note that a generic 3D point $\bm{x}$ is padded with a 1, so it is represented as
$\bm{x} = (x \  y \  z \  1 )^T$.


This is in order to work with the 4x4 transformation matrices with both rotation and translation.

\bigskip

Going back to our problem, we need to find a transformation matrix that transforms a point from \emph{Base} frame to \emph{ECEF} frame, that is noted as $\bm{T}^E_B$. Knowing that matrix, we can transform the point between frames through
\begin{equation}
 \bm{S}^E = \bm{T}^E_B \cdot \bm{S}^B
\end{equation}
This transformation can be split into two different partial transformations:
\begin{equation}
 \bm{T}^E_B = \bm{T}^E_M \cdot \bm{T}^M_B
\end{equation}

We first calculate the sensor respect to \emph{Map} frame
$ \bm{S}^M = \bm{T}^M_B \cdot \bm{S}^B $
and then we transform it in the \emph{ECEF} frame with $\bm{T}^E_M$.

\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=\textwidth]{images/impl_wolf/T_map_base_with_teo.pdf}
        \caption{Representation in 2D of the robot's frame \emph{Base} in the \emph{Map} frame.}
        \label{fig:T_map_base}
    \end{center}
\end{figure}

%% PRIMA TRASFORMAZIONE: BASE - MAP
Lets use \figurename~\ref{fig:T_map_base} to help us analyze the first transformation $\bm{T}^M_B$.
In this figure we are in the plane defined by the axes $\bm{x}^M$ and $\bm{y}^M$.
The \emph{Base} frame is translated $\bm{b}^M$ from the Map origin and rotated $\theta^M$, so the transformation matrix $\bm{T}^M_B$ will be:

\begin{equation}\label{eq:T_map_base}
\bm{T}^M_B=
  \begin{bmatrix}
    \cos(\theta^M)  &  -\sin(\theta^M) &  0  &  b_x^M  \\
    \sin(\theta^M)  &  \cos(\theta^M)  &  0  &  b_y^M  \\
    0               &   0              &  1  &  0      \\
    0               &   0              &  0  &  1      \\
  \end{bmatrix}
\end{equation}

\bigskip

%% SECONDA TRASFORMAZIONE: MAP - ECEF
The second transformation, $\bm{T}^E_M$, is conceptually similar, but it involves a conversion between ECEF and geodetic coordinates and five different frame transformations.

% ECEF to lla
First of all lets see how to convert $\bm{m}^E$, the origin of the frame \emph{Map}, from ECEF to Geodetic.
We use WGS84 as a reference ellipsoid to describe the Earth, which define the following constants:

\begin{subequations}
 \begin{align}
  \text{semi-major axis:}     \quad & a  = 6378137   \\
  \text{first eccentricity:}  \quad & e  = 8.1819190842622 \cdot 10^{-2}   \\
  \text{semi-minor axis:}     \quad & b  = \sqrt{a^2 \cdot (1 - e^2)}   \\
  \text{second eccentricity:} \quad & e' = \sqrt{\frac{a^2 - b^2}{b^2}}
   \end{align}
\end{subequations}
Then we define the following auxiliary values:
\begin{subequations}
 \begin{align}
  p & = \sqrt{(m^E_x)^2 + (m^E_y)^2}   \\
  \psi & = atan{\frac{a \cdot m^E_z}{b \cdot p}}   
 \end{align}
\end{subequations}
and finally we calculate the Geodetic coordinates of the Map origin:
\begin{subequations}
 \begin{align}
  \text{longitude:}           \quad \phi & = atan{\frac{m^E_y}{m^E_x}}   \\
  \text{latitude:}            \quad \lambda & = atan \frac{m^E_z + {e'}^2 \cdot b \cdot \sin(\psi)^3}{p - e^2 \cdot a \cdot \cos(\psi)^3}   \\
  \text{radius of curvature:} \quad N & = \frac{a}{\sqrt{1-e^2 \cdot \sin(\phi)^2}}   \\
  \text{altitude:}            \quad h & = \frac{p}{\cos{\phi}} - N
 \end{align}
\end{subequations}

%matrix chain
Remembering that $\bm{T}^E_M$ means representing the pose of \emph{Map} with respect to \emph{ECEF}, we can describe visually how we compute this computation.
The visual description is followed by the associated homogeneous transform matrix.
\begin{figure}[!htb]
   \centering
   \includegraphics[width=.49\textwidth]{images/impl_wolf/gpsCoords.pdf}\hfil
   \includegraphics[width=.49\textwidth]{images/impl_wolf/gpsCoordsB_corretto.pdf}

   \caption{Geoid and relations between  \emph{ECEF} and \emph{Map} frames {\cite{tesiAndreu}} }
   \label{fig:ecef_map}
\end{figure}

The first thing we have to do is to translate the frame from the center of the Earth to the origin of the frame \emph{Map}.
\begin{equation}
 \bm{T}^E_A=
  \begin{bmatrix}
    1   &   0   &   0   &  m_x^E  \\
    0   &   1   &   0   &  m_y^E  \\
    0   &   0   &   1   &  m_z^E  \\
    0   &   0   &   0   &  1      \\
  \end{bmatrix}
\end{equation}

After that we have to rotate the frame on the z-axis of an angle equivalent to the longitude $\phi$ of the frame \emph{Map},
\begin{equation}
 \bm{T}^A_{\phi}=
  \begin{bmatrix}
    \cos(\phi)  &  -\sin(\phi) &  0  &  0  \\
    \sin(\phi)  &  \cos(\phi)  &  0  &  0  \\
    0           &   0          &  1  &  0  \\
    0           &   0          &  0  &  1  \\
  \end{bmatrix}
\end{equation}

and then rotate it on the y-axis of an angle equivalent to latitude $\lambda$.
\begin{equation}
 \bm{T}^{\phi}_{\lambda}=
  \begin{bmatrix}
    \cos(\lambda)  &  0  &  -\sin(\lambda) &  0  \\
    0              &  1  &  0           &  0  \\
    \sin(\lambda)  &  0  &  \cos(\lambda)  &  0  \\
    0              &  0  &  0           &  1  \\
  \end{bmatrix}
\end{equation}

Now we want to exchange the axes in order to transform to East-North-Up (ENU) coordinate systems, that means to having the x-axis pointing to the East, the y-axis to the North, and the z-axis normal to the ground.
\begin{equation}
 \bm{T}^{\lambda}_{U}=
  \begin{bmatrix}
    0   &   0   &   1   &  0  \\
    1   &   0   &   0   &  0  \\
    0   &   1   &   0   &  0  \\
    0   &   0   &   0   &  1  \\
  \end{bmatrix}
\end{equation}

Finally it is necessary to apply a last rotation about the z-axis in order to align the \emph{ENU} frame with the orientation of the \emph{Map} frame.
\begin{equation}
 \bm{T}^{U}_M=
  \begin{bmatrix}
    \cos(\alpha^{U})  &  -\sin(\alpha^{U}) &  0  &  0  \\
    \sin(\alpha^{U})  &  \cos(\alpha^{U})  &  0  &  0  \\
    0             &   0            &  1  &  0  \\
    0             &   0            &  0  &  1  \\
  \end{bmatrix}
\end{equation}

In the end we have obtained the second transformation $\bm{T}^E_M$ as a chain multiplication of the presented matrices.

\begin{equation} \label{eq:T_ecef_map}
 \bm{T}^E_M = \bm{T}^E_A \cdot \bm{T}^A_{\phi}  \cdot  \bm{T}^{\phi}_{\lambda} \cdot  \bm{T}^{\lambda}_{U} \cdot \bm{T}^{U}_M
\end{equation}

Knowing this homogeneous transformation matrix we can express a generic point in Map $\bm{p}^M$ into ECEF coordinates.

\begin{equation}
 \bm{p}^E = \bm{T}^E_M \cdot \bm{p}^M
\end{equation}





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Pseudorange Constraints in Wolf}

The class \verb+ConstraintGPSPseudorange2D+ implements in the method \verb+operator()+ the math needed to add a constraint to the optimization problem.

Each parameter (or unknown) to be estimated by the optimization problem is called a \emph{State Block}, and it is contained in a Wolf class called \verb+StateBlock+.
This class is composed by a vector of variables that have to be estimated. A state block contains also the flag \verb+fixed+ that allows to enable or disable on-line the parameter in the optimization problem.
The solver considers only non-fixed state blocks, and if the flag is activated the values in the state block are considered like constants by the solver.

In table \ref{tbl:operator_variables} all the state blocks involved in GPS's \verb+operator()+ are listed.

\begin{table}[h]
\centering
\begin{tabular}{llll}
\textbf{Quantity} & \textbf{State block name} & \textbf{Dimension} & \textbf{Reference} \\ \hline
${\bm{b}}^M$   & vehicle\_p   & 2D   & Frame Map        \\
$\theta^M$     & vehicle\_o   & 1D   & Frame Map        \\
${\bm{m}}^E$   & map\_p       & 3D   & Frame ECEF       \\
$\alpha^{U}$       & map\_o       & 1D   & Frame ENU        \\
${\bm{S}}^B$   & sensor\_p    & 3D   & Frame Base       \\
$t_b$          & bias         & 1D   & GPS time       \end{tabular}
\caption{List of state blocks involved in GPS constrains.}
\label{tbl:operator_variables}
\end{table}

Applying the transformations matrices \ref{eq:T_map_base} and \ref{eq:T_ecef_map} seen in section~\ref{sec:frames}, we can compute the sensor position with respect to the \emph{ECEF} frame.
\begin{equation}
 \bm{S}^E = \bm{T}^E_M \cdot \bm{T}^M_B \cdot \bm{S}^B = \bm{T}^E_B \cdot \bm{S}^B 
\end{equation}

Then we can use this quantity to compute the expected pseudorange:
\begin{equation}\label{eq:wolf_expected_pr}
 \hat{\rho}_j = \lVert \bm{S}^E - \bm{SV}^E_j \rVert + c \cdot t_b
\end{equation}
where $\bm{SV}^E_j$ refers to the position of the j-th satellite with respect to the \emph{ECEF} frame.

Afterwards we compute the residual as the difference between the observed pseudorange and the expected one, dividing everything by the standard deviation of the measurement.
\begin{equation}
 e_j = \frac{\hat{\rho}_j - {\rho}_j}{\sigma} = \frac{\lVert \bm{S}^E - \bm{SV}^E_j \rVert + c \cdot t_b - \rho_j}{\sigma}
\end{equation}
Note that this residual involves all the quantities that we want to optimize, listed in table~\ref{tbl:operator_variables}, because $\bm{S}^E = f({\bm{b}}^M, \theta^M, {\bm{m}}^E, \alpha^{U}, {\bm{S}}^B) $.


For each pseudorange measurement available, a constraint is added to the overall optimization problem, possibly with other constraints created by different sensors.
All the constraints refer to a state of the robot in a precise time. The solver tries to find the state that minimize the overall mean squared error of the constraints connected to key-frames of the actual window~\footnote{The concepts of key-frame and window have been introduced in section~\ref{sec:wolf_teoria}}.

%\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Teo Robot and Wheel Odometry}
%more at http://wiki.iri.upc.edu/index.php/TEO
%/home/pier/Dropbox/Universita/tesi/latex/images/foto_teo/selezione
\begin{figure}[!b]
  \centering
  \subfigure[]  {\includegraphics[width=0.46\textwidth]{images/teo/teo_modello_grafica.png}}
  \hfill
  \subfigure[]  {\includegraphics[width=0.53\textwidth]{images/teo/teo_1_lq.jpg}}
  \caption{Teo Robot. In (a) we can see its RVIZ model, in (b) there is a photo taken during the experiments, where it is possible to see the mounting point of the external GPS antenna. \label{fig:teo}}
\end{figure}

In order to get some real measurements we used an IRI's mobile robot named Teo.
Teo Robot~\cite{teo-iri} is a four-wheeled Unmanned Ground Vehicle (UGV) based on the mobile robotic platform Segway RMP 400.
The robot is equipped with the electrical system, two Ubuntu-based computers, a battery, a wireless interface etc.
At perception level, Teo is equipped with a set of sensors that allow it to observe the environment. The main ones are a front and a rear laser scanners, a pair of stereo cameras, a 3D laser, a GPS receiver, a shaft encoders and an Inertial Measurement Unit (IMU). %https://en.wikipedia.org/wiki/Rotary_encoder

%TODO correzione di cui non sono proprio sicuro di aver corretto bene
%A shaft encoder is a device that measure the angular position and motion of a shaft.
A shaft encoder is not an ``absolute'' encoder, but it is just a ``relative'' encoder. So the raw measurements are counts of pulses caused by angular increments of the shaft, which jointly with vehicle kinematics allow to estimate the 2D twist ($v_x$, $v_y$, $\omega$).
Integrating the twist over time allows us to estimate a position of the robot.
Using the data produced by these devices it is possible to estimate changes in position over time.

\emph{Wheel odometry} is the process of integrating the 2D twist produced by shaft encoders, which results in a track of vehicle 2D poses referenced to an initial origin frame, usually called \emph{odometry} frame.

\begin{figure}[!htb]
    \begin{center}
	\includegraphics[width=1\textwidth]{images/teo/teo_modello_cad.jpg}
	\caption{Scheme of Teo Robot with dimensions.}
	\label{fig:teo_cad}
    \end{center}
\end{figure}

Teo is ROS-based robot and there is a ROS package~\cite{teo-ros} that contains all the nodes needed to handle it, in terms of hardware acquisition and driving.

It is manually teleoperated by a human with a Wii controller and the structure with big wheels (\figurename~\ref{fig:teo}) permits to use it also in rough grounds.

The Segway platform provides wheel odometry measurements, improved by using an IMU. The IMU sensor uses accelerometers to accurately detect the current rate of acceleration, and gyroscopes to detect changes in rotational attributes like pitch, roll and yaw.

In Teo's two-dimensional current odometry implementation, the straight movement $\Delta x$ is estimated by wheel odometry, and the yaw $\Delta \theta$ from the IMU's gyros.

All the data received by the sensors is published on different ROS topics, and the two computers mounted on board allows to process it on-line or record it for off-line processing.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Fusion Between Raw GPS and Odometry}

Wheel odometry offers a good estimation of movements in short time, but pose error irremediably drifts with time, due to integration of noisy data with time. On the other hand, GPS accuracy is poorer, but it is bounded over time.
The fusion of these two sensors overcomes the performance issues found in each individual sensor, and produces a better localization than the one offered individually during the time.

The GPS can bound the error produced by odometry in the long term, and also positions the trajectory in an absolute way with respect to the Earth, and not only with respect to the Map frame.

Odometry instead overcomes GPS weaknesses like integrity of the solution or interruption of the satellite signal due to obstructed line of sight or multipath reflections.

Moreover, with the approach of not using GPS fix but raw pseudorange data, the system is robust also in case of shading of some satellites. In fact, also if we don't receive enough pseudoranges for computing a GPS fix, they can still constrain the optimization problem, at least partially.
%%%% fine intro che sembra una conclusione %%%%

%%% Wolf_ros_gps
For these reasons, and also to be ready to fuse with other sensor modalities such as lasers or cameras, we decided to use Wolf to fuse the raw GPS pseudorange measurements with Teo's odometry.
We created a ROS node, \verb+wolf_ros_gps+, that uses the Wolf library to fuse these two sensing modalities. 

%costruttore
The constructor of \verb+wolf_ros_gps+ is appointed to create and initialize the Wolf tree. It has to create the root node \verb+WolfProblem+ and add in the hardware branch the two sensor models we want to use, \verb+SensorGPS+ and \verb+SensorOdom+, and the corresponding processors.

To help the solver converging, it is better to initialize the Map position with respect to ECEF  with a value close to the real one. This can be done using the GPS fix, if available, otherwise it is possible to discover it using a map, such as Google Maps.

The constructor initializes also Ceres-Solver through the \verb+CeresManager+ class. This object is responsible to keep the Ceres optimization problem updated, with all the fixed and non-fixed state blocks inside the actual window.

%callbacks 
When GPS or odometry measurements are published by the sensors, the respective callbacks deal with the creation of the capture objects and the addition of them in the current Wolf frame.

Odometry is published by Teo in a specific topic in the form of transformations between the parent frame \emph{Odom} and the frame \emph{Base}. 

%creazione nuovo frame
When we create a new frame, to help the convergence of the solution we want to  have an initial pose guess as close as possible to the real pose of the robot in that moment. In order to do that, if there is a previous key-frame, we select its pose as a guess for the new one. If it also has an odometry capture, we integrate it through time until the current timestamp and we have a better pose guess.
%gestione window
Every time a new frame is added to the trajectory we have to move the window and, if the window is not big enough, we have to remove the oldest frame.
This frame is only removed from the window, and not from the wolf problem. This means that its position and orientation will be considered estimated, and the constraints associated to it will not affect the following optimizations.

%PROCESS
%\hl{TODO decide and write WHEN we call process()}
Every time we want to process the arrived data we have to update the optimization problem and then solve it. Both operations are done with \verb+CeresManager+.

% risultato di process() e broadcast Odom-Map
The obtained solution after the optimization is the robot pose with respect to Map frame, $\bm{T}^M_B$.
This pose is represented by frame \emph{Base}. This frame can only have a parent, that is the \emph{Odom} frame, because it is published by Teo.
Because of that, we cannot publish directly the pose of \emph{Base} with respect to \emph{Map}. For this reason we have to make \emph{Odom} child of \emph{Map}, and publish a transform between this two frames in order to have Base in the same pose.
In \figurename~\ref{fig:frame_odom} we can see the relation between these frames.
The pose of \emph{Base} published by the odometry is $\bm{T}^O_B$.

\begin{figure}[!htb]
    \begin{center}
	\includegraphics[width=0.7\textwidth]{images/impl_wolf/frame_odom.pdf}
	\caption{Relations between \emph{Map}, \emph{Odom} and \emph{Base}.}
	\label{fig:frame_odom}
    \end{center}
\end{figure}

Through Ceres-Solver we calculate the pose of Base with respect to the map, $\bm{T}^M_B$.
To move frame \emph{Odom} from \emph{Map}, in order to have $\bm{S}^O = \bm{S}^M$, we need to calculate 
$$\bm{T}^M_O = \bm{T}^M_B \cdot \bm{T}^B_O =  \bm{T}^M_B \cdot (\bm{T}^O_B)^{-1}$$

At the beginning of the experiment the frame \emph{Map} will perfectly overlap the \emph{Odom} frame, and during the time they may separate.
The space between this two frames represents the drift of the odometry positioning respect to the positioning estimated with sensor fusion.% to the Wolf positioning.

% broadcast ECEF-Map
To connect these frames to \emph{ECEF} frame we need also to publish a transform between \emph{ECEF} and \emph{Map} frames. To do that we use the \emph{Map} position and orientation estimated and, with the math we have seen in equations~\ref{eq:T_map_base} and \ref{eq:T_ecef_map}, we can publish the transformation $\bm{T}^E_M$


%addCapture method for odometry
%If the current frame lacks of an odometry capture, the new one will be added. If there already is an odometry capture in the frame, it have to update the frame pose to the actual timestamp. In the GPS case, it will be always used the last available capture. \hl{rewrite this sentence}

%visualization of results
RVIZ will listen to the transformations published for every key-frame and visualize it, as we will see in the section~\ref{sec:wolf_result},


In \figurename~\ref{fig:wolf_tree_gps_odom} we can see the Wolf's tree specialized in the implementation of raw GPS pseudoranges and odometry.

\begin{figure}[p]
    \begin{center}
        \includegraphics[angle=90,origin=c,width=0.90\textwidth]{images/impl_wolf/wolf_tree_gps_odom.pdf}
        \caption{WOLF tree specialized with raw GPS and odometry. We are not mapping, so the \emph{Map} branch is dashed.}
        \label{fig:wolf_tree_gps_odom}
    \end{center}
\end{figure}

\clearpage
\section{Sensor Fusion Results} \label{sec:wolf_result}

In the experiments we made, we assumed to be able to estimate the ECEF coordinates where the experiment starts, in order to initialize correctly the map position $\bm{m}^E$. This assumption is not too much restrictive, because in outdoor vehicle localization we can obtain a GPS fix, and if it is not available it is possible to have a guess of the initial position using other instruments, such as Google Maps.

If the initial position is easy to obtain precisely, the map orientation $\alpha^{U}$ is definitely not. Using an incorrect value for this state block leads to a bad estimation of the vehicle position with respect to the ECEF coordinates.
With the sensor fusion we did through Wolf, the map orientation $\alpha^{U}$ can be estimated correctly, and even if it has been initialized randomly or with a value really far from the real one, it immediately converge to the correct orientation, as we can see in \figurename~\ref{fig:map_o_wrong}.

\begin{figure}[!htb]
  \centering
  \subfigure[]  {\includegraphics[width=0.49\textwidth]{images/impl_wolf/results/map_o_wrong.png}}
  \hfill
  \subfigure[]  {\includegraphics[width=0.49\textwidth]{images/impl_wolf/results/map_o_corrected.png}}
  \caption{Correction of $\alpha^{U}$. These figures plot the trajectory of the robot, in blue, and the GPS fixes produced by the GPS receiver mounted on the robot, in red. (a) shows a comparison between the two positioning without using an optimized value of $\alpha^{U}$, (b) shows the positioning obtained using sensor fusion. \label{fig:map_o_wrong}}
\end{figure}

The first goal we achieved is the absolute positioning of the map frame and the trajectory with respect to the Earth.

\bigskip

\figurename~\ref{fig:wolf_traj}(a) shows the whole trajectory, computed using only odometry, but fixing the map orientation $\alpha^{U}$ with the correct angle we estimated before. At the end of the path the estimated position of the robot is more that 5 meters far from the GPS fix produced by AsteRx1. 
%We can also notice how the odometry estimates shorter distances respect to the GPS fixes.

In \figurename~\ref{fig:wolf_traj}(b) we can see the trajectory computed by Wolf fusing odometry with raw GPS. Here the GPS constraints remove the drift of the odometry, and the final part of the trajectory is approximately $2.5$ meters far from the GPS fix.

\begin{figure}[!tbp]
  \centering
  \subfigure[Robot trajectory obtained using only odometry measurements.]  {\includegraphics[width=\textwidth]{images/impl_wolf/results/crop_odom_only.png}}
  \hfill
  \subfigure[Robot trajectory obtained fusing odometry and raw GPS measurements.]  {\includegraphics[width=\textwidth]{images/impl_wolf/results/crop_gps_odom_fused.png}}
  \caption{In blue is represented the robot's trajectory estimated by Wolf. In red there are represented the GPS fixes as comparison. The trajectory is proportional to the grid, whose cells are squares with sides of 1 meter.\label{fig:wolf_traj}}
\end{figure}

\bigskip

We have noticed that the distance covered according to the odometry sensor is in general shorter than the distance we obtain relying on GPS fix. 
In our experiment the trajectory is dominated by the odometry, but gradually merges to the measurements produced by GPS.
Fusing these kind of discording data possibly leads to problems in the estimation of the fused trajectory.
A more appropriate calibration of robot odometry system would also be necessary to improve results.

The restriction on a plane we made in section \ref{sec:frames} is fundamental to make cooperate a sensor like the GPS, that produce a three-dimensional positioning, with a sensor like wheel odometry that lie in a bi-dimensional space.

Anyway, the assumption of working on a plane tangent to the WGS84 geoid has proved to be too restrictive and creates errors in the estimation of the trajectory. This is accentuated because in the surrounding of the laboratory, where we did the experiment, there is a slope.
As we can see from \figurename~\ref{fig:altitude} the GPS fixes start in the correct position~\footnote{The GPS antenna is mounted in Teo $110$ centimeters high from the ground.} but soon they assume negative values in $z^M$ and they go under the Map plane. 

\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=0.85\textwidth]{images/impl_wolf/results/altitude.png}
        \caption{Focus on the altitude problem, caused by the plane of the experiment differently inclined with respect to Map plane.}
        \label{fig:altitude}
    \end{center}
\end{figure}

The sensor fusion we did is robust against interruption of GPS data, due to sensor breakage or obstructed line of sight between satellites and GPS antenna.

We artificially limited the number of satellites available during our experiments, to see how it deals in situations where reception is not perfect for the GPS antenna, but still can receive some GPS data. In these cases, if less that 4 satellites were available, a computation of the GPS fix would be impossible.
With our sensor fusion this problem overcame, because we can rely on the wheel odometry. 
We did a lot of tests, from which emerged that even from only two satellites measurements we still can notice a slow convergence of the trajectory to the path indicated by the GPS fixes. With only one measurements, in our experiments, we didn't noticed any effect, and the trajectory is completely dominated by odometry measurements.

