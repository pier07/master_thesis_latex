\chapter{Global Positioning System (GPS)}
\label{ch:gps}
\thispagestyle{chiara}

\section{GPS Fundamentals}
The GPS is a Global Navigation Satellite System (GNSS) that provides worldwide accurate three-dimensional position, velocity and time information to users.

It was created, and it is maintained, by the United States government. Currently GPS and the Russian GLONASS (GLObal NAvigation Satellite System) are the only fully operational GNSS.
European Union is developing another GNSS called Galileo and it is scheduled to be fully operational by 2020.

%It provides critical capabilities to military, civil, and commercial users around the world.

GPS is a dual-use system: it provides separate services for civil and military users. These are called the Standard Positioning Service (SPS) and the Precise Positioning Service (PPS) respectively. 
The SPS is designated for the civil community, whereas the PPS is intended for U.S. authorized military and select government agency users. Civilian use is permitted, but only with special U.S. Department of Defense approval.

The SPS is available to all users worldwide free of direct charges. There are no restrictions on SPS usage: it is freely accessible to anyone with a GPS receiver. This service is specified to provide accuracies of better than 13m (95\%) in the horizontal plane and 22m (95\%) in the vertical plane (global average; signal-in-space errors only). However, SPS measured performance is typically much better than specification,~\cite{foo95}.

In order to provide this information, the GPS system leans against a constellation of satellites, also called Space Vehicles (SVs).
The satellite constellation nominally consists of 24 satellites arranged in 6 orbital planes with 4 satellites per plane.
A worldwide ground control/monitoring network monitors the health and status of the satellites. This network also uploads navigation and other data to the satellites,~\cite[p.382]{hoffman}.

%TODO approfondimento su 3 segmenti: space segment + control segment and user segment?

%TODO qui potrei inserire le robe di wiki che descrivono la costellazione e l'altezza dei sats

GPS is available everywhere on the Earth and in all weather condition, provided that there is a line of sight to four or more GPS satellites in order to calculate a precise 3D position. There are techniques to obtain a position in situation where less satellites are available to the receiver, and we touch upon later on that aspects.
GPS can provide service to an unlimited number of users since the user receivers operate passively.

\bigskip

The GPS concept is based on time. %, more precisely on one-way time of arrival (TOA) ranging.
The satellites carry very stable atomic clocks that are synchronized to a GPS time base.
The synchronism between all the satellites' clock with a unique time base is maintained from ground clocks, and any drift from GPS time base is corrected daily from them.
GPS receivers have clocks as well. Usually a crystal clock is employed to minimize the cost, complexity, and size of the receiver, so it is less accurate than the satellites' clock.

GPS satellites continuously broadcast their current time and navigation data. The navigation data provides the means for the receiver to determine the location of the satellite at the time of signal transmission, whereas the ranging code enables the user's receiver to determine the transit time of the signal and thereby determine the satellite-to-user range.


A GPS receiver monitors multiple satellites and from them creates and solves a system of equations to determine the exact three-dimensional user's position.
If the receiver's clock were synchronized with the satellites' clock, only three range measurements would be required. But, because of the lower precision of crystal clocks, the receiver is not synchronized with true GPS time.
Thus, four satellites must be in line of sight with the receiver for computing the user's three-dimensional position and receiver's clock bias from GPS time. If either GPS time bias or height is accurately known, less than four satellites are required.


The three-dimensional position is calculated in Cartesian coordinates with origin in the Earth's center. The reference coordinate system chosen to represent both the satellite and the receiver is the \emph{Earth-Centered Earth-Fixed Coordinate System} (ECEF).
The receiver's Earth-centered solution location is usually converted to geodetic coordinates.

The peculiarity of ECEF is that the x-, y-, and z-axes rotate with the Earth and no longer describe fixed directions in inertial space.
The +x-axis points in the direction of 0° longitude and 0° latitude, the +y-axis points in the direction of 90°E longitude and 0° latitude, and the z-axis is chosen to be normal to the equatorial plane in the direction of the geographical North Pole, thereby completing the right-handed coordinate system.



\section{Position Determination}
\subsection{Concepts of Trilateration}
\label{sec:trilat_concepts}

To solve the user's position a trilateration technique is used, and it requires the user-to-satellite distances to be measured. These measurements are computed using time information.

The satellites embed in their signals the time of transmission. That enables the receivers to calculate the time of flight subtracting the time of transmission from the time of arrival.

Multiplying the signal's time of flight by the speed of light, the pseudorange measurement is obtained.

With only one satellite's measurement, the user would be located somewhere on the surface of a sphere centered on the satellite, as shown in \figurename~\ref{fig:sphere1sat}
%TODO vedi se affiancare figure
\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=0.3\textwidth]{images/gps/sphere1sat.pdf}
        \caption{The user is located on the surface of the sphere with center on the satellite position and radius equal to the pseudorange.}
        \label{fig:sphere1sat}
    \end{center}
\end{figure}

Using simultaneously the pseudorange of a second satellite, another sphere of possible results is found. The intersection of the two spheres shrinks the set of possible user positions to the perimeter of the shaded circle in \figurename~\ref{fig:sphere2sat}, that denotes the plane of intersection of these spheres, or at a single point tangent to both spheres (i.e., where the spheres just touch). This latter case is highly unlikely and could only occur if the user were collinear with the satellites, which is not the typical case on the Earth surface.

\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=0.6\textwidth]{images/gps/sphere2sat.pdf}
        \caption{The user is located somewhere on the perimeter of the shaded circle.}
        \label{fig:sphere2sat}
    \end{center}
\end{figure}

The plane of intersection is perpendicular to a line connecting the satellites, as shown in \figurename~\ref{fig:sat_collinear}

\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=0.5\textwidth]{images/gps/sat_collinear.pdf}
        \caption{Plane of intersection between the two spheres.}
        \label{fig:sat_collinear}
    \end{center}
\end{figure}


Adding a third satellite to this measurement process permits to complete the trilateration and determine the user position. The set of possible positions is now shrunk to the intersection of the perimeter of the circle and the surface of the third sphere.

With the intersection between the perimeter of the circle represented in \figurename~\ref{fig:sphere3sat} as shaded, and the surface of the third sphere, we obtain two points. However, only one of this two points is the correct user position.
\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=0.6\textwidth]{images/gps/sphere3sat.pdf}
        \caption{The user is in one of the two points on the shaded circle.}
        \label{fig:sphere3sat}
    \end{center}
\end{figure}
With the visual aid of the intersection given by \figurename~\ref{fig:doppia_soluz} we can observe that the candidate locations are mirror images of one another with respect to the plane generated by the 3 involved satellites.

\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=0.6\textwidth]{images/gps/doppia_soluz.pdf}
        \caption{The user is in one of the two points on circle perimeter.}
        \label{fig:doppia_soluz}
    \end{center}
\end{figure}

Assuming that the user is on the Earth's surface we can immediately discard the upper point and use the lower one as the true position of the receiver.

For spaceborne receiver the trilateration process is more complex, because they may be above or below the plane containing the satellites, and it may not be clear which point to select unless the user has supplementary information, such as tracking~\cite[pp. 21--26]{understanding_gps}. Anyway it is out of the scope of this thesis and will not be investigated.

In all of these examples the receiver clock is assumed perfectly synchronized to system time. With real GPS receivers this assumption never holds, because there are a number of error sources that affect range measurement accuracy (e.g., measurement noise and propagation delays). These can generally be considered negligible when compared to the errors experienced from non-synchronized clocks.
Therefore, in the explanation of basic concepts, errors other than clock offset are omitted. 
%TODO \hl{TODO rivedi sta conclusione, se e' il caso di lasciarla} %TODO dire se e come dopo ne terrò conto. --> in teoria ne tengo conto grazie a gpstk. dillo nel capitolo adeguato 


\subsection{Mathematical Formulation}
\label{ssec:math}
In order to deepen the analysis of position determination into mathematical terms, we can make reference to \figurename~\ref{fig:legenda}.

\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=0.6\textwidth]{images/gps/legenda.pdf}
        \caption{Relations between Earth, user and GPS satellite represented through vectors.}
        \label{fig:legenda}
    \end{center}
\end{figure}

Our goal is to determine the vector \textbf{u}, which represents a user receiver position with respect to the ECEF coordinate system origin. Thus, the user position coordinates $x_u$, $y_u$, $z_u$ are considered unknowns.

Vector \textbf{s} represents the position of the satellite relative to the coordinate origin, and the coordinates $x_s$, $y_s$, $z_s$ are computed using navigation data broadcasted by the satellite.

Vector \textbf{r} represents the vector offset from the user to the satellite and can be calculated as
\begin{equation}
 \bm{r} = \bm{s} - \bm{u}
\end{equation}

Let $r$ represent the magnitude of vector \textbf{r}.
\begin{equation}
 r = \|\bm{r}\| = \|\bm{s} - \bm{u}\|
\end{equation}

The user-to-satellite distance $r$ is computed by measuring the propagation time required by the signal sent from the satellite to reach the receiver. The propagation time is represented by $\Delta t$ and in the ideal case is: 
\begin{equation}
 \Delta t_{ideal} = t_{Rx} - t_{Tx}
\end{equation}

If the satellite clock and the receiver clock were perfectly synchronized, the correlation process would yield the true propagation time. By multiplying this propagation time, $\Delta t$, by the speed of light, the true (i.e., geometric) satellite-to-user distance could be computed. This is the ideal case as described in \sectionname~\ref{sec:trilat_concepts}, but, as stated before, the satellite and receiver clocks are generally not synchronized. The receiver clock will usually have a bias error from system time that we will call $t_b$, which is unknown. Taking into consideration this quantity we obtain the propagation time in the non-ideal case:

\begin{equation}
 \Delta t = t_{Rx} - t_{Tx} + t_b
\end{equation}

Thus, the range determined by the correlation process is denoted as the pseudorange $\rho$, because it is the range determined by multiplying the signal propagation velocity, \textit{c}, by the time difference between two non-synchronized clocks (the satellite clock and the receiver clock).

This $\rho$, calculated as $\rho = \Delta t \cdot c $, is considered as the \emph{measurement} received from the satellite.


\subsubsection{Calculation of User Position}
In order to determine user position in three dimensions $(x_u, y_u, z_u)$ and the time bias $t_b$, pseudorange measurements $\rho_j$ are required from four satellites resulting in the next system of equations, where $j$ references each current satellite. 

Given the user position $\bm{u}$, the receiver bias $t_b$ and the $j$-th satellite position $\bm{s}_j$, the expected pseudorange $\hat{\rho}_j$ is

\begin{equation}
 \hat{\rho}_j =  \| \bm{s}_j - \bm{u} \| + c \cdot t_b
\end{equation}

This equation can be expanded in $x_u$, $y_u$, $z_u$ and $t_b$:

\begin{equation}\label{eq:expected_pr}
 \hat{\rho}_j = \sqrt{ (x_{s_j} - x_u)^2 + (y_{s_j} - y_u)^2 + (z_{s_j} - z_u)^2 } + c \cdot t_b
\end{equation}

After that we can compute the error as the difference between received and expected measurement, $\rho_j$ and $\hat{\rho}_j$:

\begin{equation} \label{eq:pr_difference}
 e_j = \rho_j - \hat{\rho}_j    \qquad \qquad   j \in \{Currently \; tracked \; satellites\}
\end{equation}
Note that in this equation the unknowns are four: the user position $x_u$, $y_u$, $z_u$ and the time bias $t_b$.

To find the optimal result for this four unknowns, we have to minimize the sum of squared errors corresponding to the satellites observed in that epoch.

\begin{multline}\label{eq:trilat}
(x_u, y_u, z_u, t_b)^*
= \operatorname*{arg\,min}_{(x_u, y_u, z_u, t_b)} \sum_j \left(  \hat{\rho}_j - \rho_j  \right)^2 \\
= \operatorname*{arg\,min}_{(x_u, y_u, z_u, t_b)} \sum_j \left(  \sqrt{ (x_j - x_u)^2 + (y_j - y_u)^2 + (z_j - z_u)^2 } + c \cdot t - \rho_j  \right)^2 
\end{multline}
where $x_j$, $y_j$, $z_j$ denote the $j$-th satellite's position in three dimension.

When more than four satellites are available, we can choose if using only the four more reliable measurements or use more than four simultaneously.

\section{GPS Signals}
%vedi anche pg. 35 librone understanding gps

In the previous section we assumed to know satellites' position and pseudoranges every time we want to trilaterate the receiver position. 
In order to provide these information satellites broadcast two different signals, called respectively observation and navigation data.

\subsection{Observation Data}
\label{ssec:obs_data}
Observation data contains a ranging code that enables the user’s receiver to determine the propagation time of the signal, and thereby determine the satellite-to-user \emph{pseudorange}.
In order to do that, GPS observables include three fundamental quantities that need to be defined:

\begin{itemize}
 \item \emph{Time}: The time of the measurement is the receiver's time of the received signals. It is identical for all the satellites observed in a observation, and it is also called \emph{epoch}. It is expressed with respect to GPS time and not to Coordinated Universal Time UTC.
 \item \emph{Pseudorange}: The pseudorange is the measured distance from the receiver antenna to the satellite antenna, including receiver clock bias and other error sources such as atmospheric delays. It is measured comparing two clocks but it is converted and stored in units of meters, multiplying the time measurement by the speed of light.
 \item \emph{Phase}: The phase is the carrier-phase measured in whole cycles at the frequencies used to transit the signal. The original civil signal is transmitted using two different bands: 1575.42 MHz called L1 and 1227.60 MHz called L2~\cite{gpsFreq}. Anyway, not all the receivers track both signals.
\end{itemize}

%TODO cita https://igscb.jpl.nasa.gov/igscb/data/format/rinex210.txt

Observation data messages are kept very small for the sake of being computed at a high rate by the receiver.


\subsection{Navigation Message}
\label{ssec:nav_msg}

In addition to the pseudoranges, a receiver needs to know detailed information about each satellite's position and the full GPS constellation. 
This information is sent to receivers from satellites through the navigation message, and allows the users to compute the satellite's orbit and the precise position at each moment, to perform the positioning calculus when observation message arrives.


%\hl{ALTERNATIVA ALLA FRASE SOPRA}
%This information is provided to receivers from satellites through the navigation messages. Each one of this message contains the orbital parameters needed to describe the true position and velocity of the satellite at the reference time, and how this can vary 
%At the exact reference time, the reference orbital parameters will describe the true position and velocity vectors of the satellite, but as time progresses beyond (or before) the reference time, the true position and velocity of the satellite will increasingly deviate from the position and velocity described by the six two-body integrals or parameters.
%This is the approach taken in formulating the GPS ephemeris message, which includes not only six integrals of two-body motion, but also the time of their applicability (reference time) and a characterization of how those parameters change over time.
%With this information, a GPS receiver can compute the “corrected”  integrals of motion for a GPS satellite at the time when it is solving the navigation problem. 


The navigation message includes:
\begin{itemize}
 \item \emph{Time Parameters and Clock Corrections}: to compute satellite clock bias and time conversions.
 \item \emph{Ephemeris Parameters}: to precisely estimate the satellite's orbit during time and compute the satellite coordinates with enough accuracy. Each satellite transmits its own ephemeris.
 \item \emph{Service Parameters}: that contain satellite health information.
 \item \emph{Ionospheric Parameters Model}: to correct the ionospheric effect on single frequency observations.
 \item \emph{Almanacs}: permit the computation of the position of the satellites constellation with the reduced accuracy of 1-2 km of 1-sigma error, which assists the receiver in determining which satellites search for. Each satellite transmits almanac data for several satellites.
\end{itemize}

In order to being able to describe almanacs and to maintain a highly accurate time synchronization between the space vehicles, every satellite exchanges information with fixed ground stations on Earth's surface. 
GPS ephemeris message includes not only parameters to calculate satellites position, but also the time of their applicability. The ephemerides and clock parameters are usually updated every two hours, but anyway they are considered still valid for four hours. Instead the almanac is valid for six days.~\cite{navMsg}

Navigation data is heavier than observation data, and it changes every two hours. For these reasons it has been chosen to send this data in a different message, separated from observables, and at the considerably slower rate of one every thirty seconds. Thus, when a GPS sensor is turned on, it have to wait maximum thirty seconds in order to be able to start trilaterating the user position, if at least four satellites are visible.

The whole navigation message consists of 25 frames of 30 seconds each. A frame is 1500 bit long, and is subdivided in 5 sub-frames of 300 bits, numbered 1 to 5. In turn each sub-frame consists of 10 words of 30 bit each and requires 6 seconds to transmit (see \figurename~\ref{fig:nav_msg}). Each sub-frame has the GPS time.
The first sub-frame contains the GPS week number and information to correct the satellite's time to GPS time, plus satellite status and health. Sub-frames 2 and 3 together contain the transmitting satellite's ephemeris data. Sub-frames 4 and 5 contain only 1/25th of the complete almanac. A receiver must process 25 whole frames to retrieve the entire 15,000 bit almanac message. At this rate, 12.5 minutes are required to receive the entire almanac from a single satellite.


\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=0.9\textwidth]{images/gps/nav_msg.png}
        \caption{Structure of the navigation message.}
        \label{fig:nav_msg}
    \end{center}
\end{figure}

A short discussion is needed to justify the ionospheric parameters. The signals from the GPS satellites are perturbed as they transit the ionosphere. Pseudorange measurements are increased in value because an additional delay is added to the time of flight. For this reason a correction is needed, otherwise the positioning would not be accurate.

If the receiver works with both frequencies $L1 = 1575.42 MHz$ and $L2 = 1277.60 MHz$, a linear combination of the measurements removes almost all of the ionospheric perturbations~\cite{ionosphere}.
Lets call $\rho^{L1}$ and $\rho^{L2}$ the pseudoranges measured respectively on  $L1$ and $L2$, it is possible to correct the measurements applying a ionospheric correction $\Delta_{ionospheric}$ in this way:
\begin{equation}
 \tau = \left(\frac{L1}{L2}\right)^2
\end{equation}
\begin{equation} \label{eq:iono_corr}
 \Delta_{ionospheric} = \frac{\rho^{L1}-\rho^{L2}}{1-\tau}
\end{equation}
\begin{equation}
 \rho_{corrected} = \rho^{L1} - \Delta_{ionospheric}
\end{equation}


If the receiver works with only one frequency it is relevant to apply a precise ionospheric model. This model can be built from the parameters included in the navigation message.

\ifjunk
 %The ionosphere – that region of the upper atmosphere where free electrons exist in sufficient numbers to affect the propagation of radio waves – owes its existence primarily to the extreme ultraviolet (EUV) and x-ray photons emitted by the sun. They ionize atoms and molecules in the upper atmosphere, freeing the outer electrons. Mostly the ionosphere is well behaved but it can get quite interesting when it is disturbed by space weather events such as solar flares or coronal mass ejections.
 
 %The signals from the GPS satellites are perturbed as they transit the ionosphere. Pseudorange measurements are increased in value (an additional delay) and carrier-phase measurements are decreased (a phase advance). 
 %If not fully modeled or otherwise accounted for, the perturbations can decrease the accuracy of GPS positioning, navigation, and timing (PNT). 
 %For highest PNT accuracies, observations are made at the two frequencies transmitted by all GPS satellites and because the ionosphere’s effect on radio signals is dispersive, a linear combination of the measurements removes almost all of the ionospheric perturbations.
 %On the other hand, the ionosphere’s effect on single frequency observations must be corrected using a model. Most commonly, the model assumes that all of the electrons in the ionosphere can be compressed into a thin shell at a certain height above the receiver. This permits the computation of an estimate of the vertical ionospheric delay. Then, a mapping function is used to predict the slant delay, the delay contributing to a GPS measurement. The approach works reasonably well, particularly if near-real-time values of vertical delay can be provided to users as is done by the Wide Area Augmentation System and other satellite-based augmentation systems. However, this two-dimensional approach ignores the fact that the electron content of the ionosphere is actually spread out in the vertical direction and so has certain inaccuracies, which can increase when the ionosphere is disturbed.

 %http://gpsworld.com/innovation-ionospheric-modeling-using-gps/
 
\fi

%questa è la modernizzazione, ma è giusto un accesso per cultura generale (a me non interessa)
The navigation message format described previously is called L1 C/A navigation message, and represents the legacy message. Nowadays the system has been modernized and new type of messages has been introduced: L2-CNAV, CNAV-2, L5-CNAV and MNAV. Anyway, in modernized GPS, the legacy navigation message is still used, but transmitted at a higher rate and with improved robustness, and the new messages contains additional data for civil or military users~\cite{navMsg}.

