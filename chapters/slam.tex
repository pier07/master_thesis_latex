\chapter{Sensor Fusion in Mobile Robotics}
\label{ch:slam_and_sensor_fusion}
\thispagestyle{chiara}

\section{Simultaneous Localization And Mapping}
\label{sec:slam}
Simultaneous Localization And Mapping (SLAM) is the computational problem of incrementally constructing a consistent map of an unknown environment with a moving robot, while simultaneously determining its location within this map.

The SLAM problem is a key factor in all mobile robotics fields, in fact a solution to this problem would provide the means to make a robot truly autonomous~\cite{dissana06}.

The problem of robot localization in unknown environments is essential in any task that require motion, and the capability to build a map helps the robot to plan its movements according to the environment (obstacle avoidance, motion planning, robot learning etc.).

Nowadays SLAM is employed in domestic, industrial, health-care, military and security settings. Some examples are self-driving cars, unmanned aerial vehicles, autonomous underwater vehicles, planetary rovers, human body inspection, until domestic tasks such as vacuuming or gardening.

When a mobile vehicle traverses an unknown environment, it measures its own movements, and detects external objects or features in this environment. These detections are called relative observations of landmarks. Using this data the vehicle builds a map and use it to get localized in it.

\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=\textwidth]{images/sensor_fusion/slam_problem_essential.pdf}
        \caption{Simultaneous estimation of both robot trajectory and landmarks locations. It also shows how landmarks location is affected by the error in vehicle positioning.}
        \label{fig:slam_essential}
    \end{center}
\end{figure}

In SLAM both the trajectory of the platform and the location of all landmarks are estimated on-line without the need of any prior knowledge of location.

\figurename~\ref{fig:slam_essential} represents the essential SLAM problem: a mobile robot, represented as a triangle, moving through the environment using a sensor to take relative observations (red arrows) of landmarks (stars)~\cite{slam_essential, graph-slam, solaSlam}.

At time $k$, the following quantities can be defined:
\begin{itemize}
 \item $\bm{x}_k$: State vector that describe the pose of the robot. Pose means position and orientation.
 \item $\bm{l}_i$: A vector describing the location of the \emph{i-th} landmark.
 \item $\bm{u}_k$: The control vector applied at time $k - 1$ to drive the robot to the state $\bm{x}_k$ at time $k$ from its previous state $x_{k-1}$.
 \item $\bm{z}_{ik}$: Observation of the \emph{i-th} landmark at time $k$.
\end{itemize}
%
SLAM problem can be formalized in a probabilistic form~\cite{solaSlam, slam-accuracy}. Each of the just defined quantities is a random variable, and they can be grouped in the following sets:
\begin{itemize}
 \item $X = \{ \bm{x}_0, \bm{x}_1 \dots , \bm{x}_k  \} $: The history of robot's poses. $X_{0..k}$ represents the robot's states from time $0$ to time $k$, and it is also called trajectory of the robot.
 \item $L = \{ \bm{l}_1, \bm{l}_2 \dots , \bm{l}_n  \} $: The sets of all landmarks (also known as map).
 \item $U = \{ \bm{u}_1, \bm{u}_2 \dots , \bm{x}_k  \} $: The history of control inputs.
 \item $Z = \{ \bm{z}_1, \bm{z}_2 \dots , \bm{z}_k  \} $: The sets of all landmark observations.
\end{itemize}

The pose of the robot at time $k$, $\bm{x}_k$, depends on the pose of the robot at the previous time, $\bm{x}_{k-1}$, and of the control $\bm{u}_k$ given at the robot.
\begin{equation}
 P(\bm{x}_k \mid \bm{x}_{k-1}, \bm{u}_k)
\end{equation}

An observation of a landmark, $\bm{z}_i$, depends on the pose the robot had when the measurement has been taken, $\bm{x}_k$, and on the pose of the observed landmark, $\bm{l}_i$. 
\begin{equation}
 P(\bm{z}_i \mid \bm{x}_{k}, \bm{l}_i)
\end{equation}

The joint probability of trajectory, controls and measurements is the product of all these probabilities.
\begin{equation} \label{eq:slam_likelihood}
 P( X, L, U, Z) \propto P(\bm{x}_0)  \prod_k P(\bm{x}_k \mid \bm{x}_{k-1}, \bm{u}_k) \prod_i P(\bm{z}_i \mid \bm{x}_{k}, \bm{l}_i)
\end{equation}

A SLAM problem consists in maximizing the likelihood of the trajectory and landmark poses, so it means finding the sets $X^*$ and $L^*$ that maximize equation~\ref{eq:slam_likelihood} which implies to find that state (poses and landmarks) that explains better all the measurements gathered during the movement.
\begin{equation}
 \{ X^* , L^* \} = \operatorname*{arg\,max}_{X, L} \left( P(\bm{x}_0)  \prod_k P(\bm{x}_k \mid \bm{x}_{k-1}, \bm{u}_k) \prod_i P(\bm{z}_i \mid \bm{x}_{k}, \bm{l}_i) \right)
\end{equation}

These dependencies can be well represented by a factor graph (\figurename~\ref{fig:slam_factor_graph}). A factor graph is a bipartite graph representing the factorization of a function. In this graph there are two kind of vertices: 
variable vertices, that represent the state of the variables we want to estimate, 
and factor vertices, which are the constraints between the states.


\begin{figure}[!htb]
    \begin{center}
        \includegraphics[width=0.75\textwidth]{images/sensor_fusion/slam_factor_graph.pdf}
        \caption{Factor graph of a SLAM problem \cite{solaSlam}. Capital letters on the left of the graph indicate the set of random variables in which the nodes belong. }
        \label{fig:slam_factor_graph}
    \end{center}
\end{figure}

Each state block is constrained to a small number of other blocks compared to the cardinality of the graph, so the resulting graph is called sparse. This sparsity is taken into account to solve the associated non-linear optimization problem with the most appropriate algorithms for this kind of problems, such as the incremental Cholesky factorization method~\cite{solaSlam}.

%\url{https://github.com/IRI-MobileRobotics/wolf/wiki/Introduction-to-sparse-nonlinear-optimization}

%In \figurename~\ref{fig:cargo_ants_slam} we can see an example of SLAM in the Cargo-ANTs project. It is represented in yellow In a freight terminal
\begin{figure}[!htbp]
    \begin{center}
        \includegraphics[width=\textwidth]{images/sensor_fusion/cargo_ants_slam.png}
        \caption{Example of SLAM in Cargo-ANTs. The vehicle, represented by a yellow rectangle, is moving in a freight terminal. During the time it builds a map, discovers landmarks (in blue) and creates constraints (yellow lines) between landmarks and the poses in its trajectory.}
        \label{fig:cargo_ants_slam}
    \end{center}
\end{figure}

\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Sensor Fusion}
\label{sec:fusion}
When the robot moves in the map, it tries to localize itself in it observing landmarks and sensing its own motion.
These observations, also called measurements, can be done with one sensor, but also with more sensors of the same kind, or with different types of sensors.

%definition:
\textbf{Sensor fusion} means combining data from a set of heterogeneous or homogeneous sensors into a coherent and enhanced description of the surrounding environment.

A system that uses sensor fusion expects a series of advantages with respect to a single sensor system:

\begin{itemize}
 \item Improved accuracy: When multiple independent measurements of the same property are fused, the accuracy of the resulting value is better than the one achieved with a single sensor.
 \item Robustness and reliability: Multiple sensor suites have an inherent redundancy which enables the system to provide information even in case of partial failure.
 \item Robustness against measurements outliers: By increasing the dimensionality of the measurement space the system becomes more robust to outliers (non coherent measurements).
 \item Reduced ambiguity and uncertainty: Joint information reduces the set of ambiguous interpretations of the measured value.
 \item Extended spatial and temporal coverage: One sensor can look where others cannot, or respectively can perform a measurement while others cannot.
 \item Increased confidence: A measurement of one sensor is confirmed by measurements of other sensors covering the same domain. 
\end{itemize}


Sensor fusion is not a new concept, and theoretically exists from a long time, but nowadays the advances in sensor technology and processing techniques, combined with improved hardware, make real-time fusion of data possible in complex applications such as SLAM.

\clearpage
\section{Windowed Localization Frames (WOLF)} %https://github.com/IRI-MobileRobotics/wolf/wiki
\label{sec:wolf_teoria}

The effort required to build and manage SLAM and sensor fusion is often huge. For that reason Wolf has been created.
The acronym Wolf stands for Windowed Localization Frames and it is a versatile software framework for the localization of a mobile robot and mapping of the environment around it~\cite{wolf}.

Wolf has been created in the IRI's group of mobile robotics, where it is currently being designed, improved and developed to offers solutions to a wide range of SLAM problems.
Wolf development started at the same time of the Cargo-ANTs project, which is one of the possible applications. Anyway, Wolf is thought to be as general as possible, to address SLAM problem in many different contexts.

The philosophy behind Wolf is providing a framework that automates the basic functionality needed in a SLAM problem, freeing developers to focus on the engineering of localization solutions and not on the low level coding.

Wolf allows to configure a robotic system with virtually any number of sensors, of any kind, an make them work together. 

The main structure in Wolf is a tree  of base classes that describe the common elements of a SLAM problem (see \figurename~\ref{fig:schema_wolf}).
%
\begin{figure}[p]
    \begin{center}
        \includegraphics[angle=90,origin=c,width=0.85\textwidth]{images/sensor_fusion/Wolf_structure.pdf}
        \caption{WOLF tree: a structure for storing all the information of the SLAM problem. Base classes are Sensor, Processor, Landmark, Capture, Feature and Constraint. For illustrative purposes, this diagram shows a case where the problem is solved with cameras and IMU, but Wolf is generic to which sensor modality is fused.}
        \label{fig:schema_wolf}
    \end{center}
\end{figure}
%
The root of the tree is the \emph{Wolf Problem}, from where three main branches start: \emph{Hardware}, \emph{Trajectory} and \emph{Map}.

The \emph{Hardware} branch contains a list of the utilized sensors with their parameters. The parameters that describe a property of the sensor, like time bias for an internal clock or focal length for a camera, are called intrinsic.
Instead, the parameters that are not proper of the sensor, like the mounting 3D pose on-board a vehicle, are called extrinsic.
The \emph{Hardware} branch does not manage real sensor data acquisition, which is out of the the Wolf scope.

Each sensor is associated with one or more \emph{Processor}, which is a class appointed to process the received raw data (called Capture, see below).

Another important branch is the \emph{Map}. A map is composed by a set of \emph{Landmarks}, that are features that describe the surrounding environment.

Every Wolf problem has a \emph{Trajectory}, that is a list of reference \emph{Frames} that define the state of the robot at a different time. Each frame has a list of \emph{Captures}, that are objects where the raw data captured is stored.

The signal processor associated to the sensor that took a capture process the capture itself, and extract a set of metric measurements, called \emph{Features}.

Each \emph{Feature} leads to a list of \emph{Constraints} for the optimization problem. With a \emph{Constraint} it is possible to compute a residual, that is a discrepancy between the received measurement and the prediction of this measurement we can make based on the robot estimated state.

Wolf is said windowed because the optimization problem can be restricted to only a subset of all the frames in the whole trajectory. The frames in this window, that are the ones that enter into the optimization problem, are called \emph{Key-Frames}.
By minimizing with a solver all the residuals associated to these key-frames we can compute the optimal state of the robot, that is the goal of the localization problem.

Wolf is mainly a structure to store data in an organized way. The task of solving an optimization problem is left to an external solver, that can be integrated with Wolf through a wrapper. Natively, Wolf provides a ready-to-use wrapper for Ceres-Solver~\cite{ceres-solver}, a solver created by Google that will be further described in section~\ref{sec:ceres}.

Chapter~\ref{ch:impl_wolf} of this report presents in detail how Wolf base classes have been specialized to solve the fusion problem of interest in this work, which is the fusion of an odometry source with raw GPS pseudorange measurements.
