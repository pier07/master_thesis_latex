# Tools configuration
#### tex editor: KILE
 go to ***settings - configure Kile - tools - build***:

- PDFLaTeX:
    - select: modern
    - command: pdflatex
    - options: -synctex=1 -interaction=nonstopmode '%source'

- ViewPDF: 
  - select: okular
    - command: okular
    - options: --unique '%target'

#### pdf view: OKULAR
go to ***settings - configure okular - editor***:
- editor: kile
- command: kile --line %l

----

# Recaps
#### Short recap: (300 characters)
We developed a software able to establish geometric constraints for a localization problem from raw GPS measurements. Then we integrated it in Wolf, a software framework for managing SLAM, enriching its sensor fusion capabilities. In the end we tested the sensor fusion between raw GPS and odometry.

#### Medium recap: (300 words, can be an abstract)
This dissertation outlines the benefits that sensor fusion brings in the mobile robotics problem of incrementally constructing a consistent map of an unknown environment with a moving robot, while simultaneously determining its location within this map.

It focuses on outdoor localization of autonomous vehicles, and aims to use raw GPS pseudorange data to establish geometric constraints to be added in the localization problem.
This problem can be constrained by constraints imposed by different kind of sensor, achieving sensor fusion.

We applied the expertise acquired from an introductory study on the GPS technology to develop a software framework that permits to interface with a real GPS receiver, process the raw data captured with all the corrections needed, and build the optimization problem.

We integrated our work in Wolf, a software framework for managing SLAM, enriching its sensor fusion capabilities.

We tested our integration fusing raw GPS measurements with the odometry produced by the wheels of a robot.

Thanks to the sensor fusion, we are able to give an absolute positioning to the robot's trajectory, which is smoother than the trajectory estimated using only raw GPS. 
The GPS constraints remove the drift of the odometry, fixing one of its biggest flaw.
On the other hand, odometry adds robustness to the localization system in case of interruption of GPS data, due to sensor breakage, temporary obstructed line of sight with satellites or also poor reception, when only few satellites are available, and they are not enough to calculate a GPS fix.

In the end we draw future developments we could do to improve our solution. The most important aspect we have to tackle is to enhance the outlier detection, to discard or improve faulty GPS measurements, especially taking into account multi-path interference in GPS signals.


#### Keywords
mobile robotics, SLAM, Wolf, sensor fusion, GPS, raw, raw GPS, pseudorange, odometry, autonomous vehicle, localization, ROS, ceres, GPSTk
