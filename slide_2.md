#intro
## motivation
* _importance of SLAM in mobile robotics_

## objective
* _sensor fusion with raw gps_
* importanza sensor fusion
* perche fondo raw gps e non GPS normale?
* cosa può dare il raw gps in un sensor fusion problem

##use case istance
* cargo ants

##SLAM (piu in dettaglio)
* immagine 2.1
* definizione statistica 
* problema di ottimizzazione

##sensor fusion
..se c'è altro da dire

##Wolf
* acronimo
* utilità
* descrizione a grandi linee dell'albero

---
#GPS teoria
> we wanted to add GPS in wolf, so we had to learn how GPS works:

## GPS intro
* acronimo
* intro
* gps is based on time

##trilateration
* ideal case
* receiver's clock bias
* mathematical formulation (serve introdurre ECEF)
	* calcolo expected pr
	* received pr (che verrà corretto, ma ne parlo dopo)
		* applico correzione ionosferica (eq. 3.11 pg 22)
		* e anche quella relativistica (eq 4.10 pg 34)
* argmin somma quadrati degli errori... [eq 3.8]

##GPS signals
* obs
* nav

----

# Implementazione GPS data processing
* introduci ros
* schema architettura
* 

## analisi architettura:

### input
* rinex
* real GPS receiver
	* intro a asterx_1
	* custom driver

	
### elaborazione input
* compute sat position from ephemeris
* correzione received pr measurements
	* iono
	* relativistic
		* se avessimo 2 freq
		* noi applichiamo un modello
* outliers rejection 
	* RAIM

###  trilaterate real data through ceres
* intro ceres
* non linear least squares problem nella forma accettata da ceres

## risultati trilat real data
* analisi fatta bene

----

# integrazione in wolf

##wolf piu in dettaglio
* come è stato implementato 
	* constraints vogliono implementato operator()
	* differenza con operator() semplice di trilateration
	* la differenza è dovuta al fatto che cerchiamo sensor_p_ecef e wolf lo considera wrt la base del robot base.
	* introduci i frame

## reference frame structure
* concetto di frame
* 4 frames necessari
* trasformazioni fra essi
* T^E^_M  * T^M^_B

## pseudorange constraint in wolf e operator()

----
# TEST fusione: raw GPS e odom

## TEO
* intro a teo

## Wheel odometry
* come funziona (magari anche  nella stessa slide di teo)

## fusione
* pro / contro
* cioè implementazione del nodo wolf_ros_gps
* dettaglio frame odom e rapporto con quello che risolve il solver
* ***sensor fusion results***
	* assumptions
	* achieved results
	* problemi

	
----
# Conclusioni
* 
* 
* 

##future developments
* 
* 
* 

----

----

# Domande a cui devo preparare risposta
* è possibile usare gps fix anche con meno di 4 satelliti disponibili, assumento altezza o clock bias uguali all'ultimo fix. cosa può dare in piu l'approccio raw?
* come posso usare un fix in wolf sensor fusion?
* io presento la trilaterazione nel caso ideale, senza time bias. cosa cambia se lo considero? clock bias aggiunge una variabile in più da stimare.
* è l'errore nei pseudorange (dovuto ai ritardi aggiunti ecc) a causare incertezza
* perchè non hai fatto esperimenti piu significativi, tipo path circolare o giro piu lungo?
* perchè nel residual in wolf consideri anche la varianza e prima non la consideravi? (eq. 5.16 pg. 46) ---> perchè in wolf vado a comparare errori misurati da diversi sensori, e avranno misure quantitativamente diverse. trasformo queste misure in grandezze probabilistiche, dividendo per std dev della capture
* parati il culo per la frase "distanza odom è piu corta che quella gps"
* 
